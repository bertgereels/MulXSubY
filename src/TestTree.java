import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class TestTree {
	
	@Test
	public void testGetRoot() {
		Tree<String> testTree = new Tree<String>("hallo");
		assertEquals("hallo", testTree.getRoot().getElement());
	}
	
	@Test
	public void testAddChildren() {
		Tree<String> testTree = new Tree<String>("hallo");
		try {
			testTree.addChild(testTree.getRoot(), "goedendag");
			testTree.addChild(testTree.getRoot(), "hello");
			testTree.addChild(testTree.getRoot(), "hey");
		} catch (InvalidTreePositionException e) {
			e.printStackTrace();
		}
		assertEquals("goedendag", testTree.getRoot().children().get(0).getElement());
		assertEquals("hello", testTree.getRoot().children().get(1).getElement());
		assertEquals("hey", testTree.getRoot().children().get(2).getElement());
	}
	
	
	@Test
	public void testBreadthFirst() {
		Tree<String> testTree = new Tree<String>("hallo");
		try {
			testTree.addChild(testTree.getRoot(), "goedendag");
			testTree.addChild(testTree.getRoot(), "hello");
			testTree.addChild(testTree.getRoot(), "hey");
		} catch (InvalidTreePositionException e) {
			e.printStackTrace();
		}
		
		try {
			testTree.addChild(testTree.getRoot().children().get(0), "testje");
		} catch (InvalidTreePositionException e) {
			e.printStackTrace();
		}
		
		ArrayList<TreePosition<String>> temp = new ArrayList<TreePosition<String>>();
		TreePosition<String> tempRoot = testTree.getRoot();
		temp = testTree.breadthFirst(tempRoot, temp, true, 0);
		
		assertEquals("hallo", temp.get(0).getElement());
		assertEquals("goedendag", temp.get(1).getElement());
		assertEquals("hello", temp.get(2).getElement());
		assertEquals("hey", temp.get(3).getElement());
		assertEquals("testje", temp.get(4).getElement());	
	}

}
