import java.util.ArrayList;

public class Tree<E>{
	private TreeNode root;
	private int size;
	
	public Tree() {
		root = null;
	}
	
	public Tree(E element) {
		root = new TreeNode(element);
	}
	
	public int size() {
		return size;
	}
	
	/**
	 * Breadth-First algorithm to traverse the tree.
	 * @param root
	 * @param list
	 * @param isRoot
	 * @param previousNodeCount
	 * @return ArrayList with the treepositions added as the algorithm traverses the tree.
	 */
	public ArrayList<TreePosition<E>> breadthFirst(TreePosition<E> root, ArrayList<TreePosition<E>> list, boolean isRoot, int previousNodeCount){
		int nodeCount=0;
		
		if(isRoot) {
			list.add(root);
		
			for(TreePosition position : ((TreeNode)root).children) {
				list.add(position);
				nodeCount++;
			}
			
			breadthFirst(root, list, false, nodeCount);
			
		}else {
			for(int i = list.size()-previousNodeCount; i < list.size(); i++) {
				TreeNode temp = (TreeNode)list.get(i);
				
				for(TreePosition position : ((TreeNode)temp).children) {
					list.add(position);
					nodeCount++;
				}
				
			}
			if(nodeCount != 0) {
				breadthFirst(root, list, false, nodeCount);
				
			}
		}
		return list;
	}
	
	public ArrayList<TreePosition<E>> preorder(TreeNode current, ArrayList<TreePosition<E>> list){
		list.add(current);
		for(TreePosition node : current.children) {
			preorder((TreeNode)node, list);
		}
		return list;
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	public TreePosition<E> getRoot() {
		return root;
	}
	
	public void addChild(TreePosition<E> pos, E element) throws InvalidTreePositionException {
		TreeNode node = validate(pos);
		node.addChild(element);
		size++;
	}
	
	private TreeNode validate(TreePosition<E> pos) throws InvalidTreePositionException{
		if(pos != null || pos instanceof Tree.TreeNode) {
			return (TreeNode) pos;
		}
		throw new InvalidTreePositionException();
	}

	private class TreeNode implements TreePosition{
		private TreeNode parent;
		private E element;
		//private ArrayList <TreeNode> children;
		private ArrayList <TreePosition<E>> children;
		
		/**
		 * Constructor overloading
		 */
		public TreeNode(E element) {
			this(null, element);
		}
		
		public TreeNode(TreeNode parent, E element) {
			this.parent = parent;
			this.element = element;
			children = new ArrayList<TreePosition<E>>();
		}

		@Override
		public E getElement() {
			return element;
		}
		
		public void addChild(E element) {
			children.add(new TreeNode(this, element));
		}
		
		public int level() {
			TreeNode current = this;
			int level = 0;
			while(!current.isRoot()) {
				level++;
				current = current.parent;
			}
			return level;
		}
		
		public int levelR(TreeNode current) {
			if(current.isRoot()) return 0;
			return levelR(current.parent) +1;
		}
		
		public boolean isRoot() {
			return parent == null;
		}

		@Override
		public TreePosition parent() {
			if(parent != null) {
				return parent;
			}
			return null;
		}

		@Override
		public ArrayList<TreePosition<E>> children() {
			if(children.size() != 0) {
				return children;
			}
			return null;
		}
	}

}
