import java.util.ArrayList;

/**
 * Represents a position in the tree.
 *
 * @param <E> type of the element at the position in the tree
 */
public interface TreePosition<E> {
	/**
	 * 
	 * @return the element at the position in the tree
	 */
	E getElement();
	
	TreePosition<E> parent();
	
	ArrayList<TreePosition<E>> children();

}
