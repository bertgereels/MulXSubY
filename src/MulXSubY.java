import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class MulXSubY {
	private int X;
	private int Y;
	private int treeLayers;
	private int calcCounter;
	private Tree<Integer> calculationTree;
	Scanner reader = new Scanner(System.in); 
	
	/**Constructor 1
	 * 
	 * @param X
	 * @param Y
	 * @param treeLayers
	 */
	public MulXSubY(int X, int Y, int treeLayers) {
		this.X = X;
		this.Y = Y;
		this.treeLayers = treeLayers;
		calcCounter = 0;
		calculationTree = new Tree<Integer>(X);
	}
	
	/**
	 * Constructor 2
	 */
	public MulXSubY() {
		calcCounter = 0;
		promptUserForInput();
		calculationTree = new Tree<Integer>(X);
	}
	
	/**
	 * Make new MulXSubY object and construct a tree with as root the starting number
	 * @param args
	 */
	public static void main(String [ ] args) {
		MulXSubY problem = new MulXSubY();
		problem.populateTreeWithCalculations();
	}
	
	/**
	 * Method that populates the tree with given amount of layers.
	 * The amount of layers is entered by the user.
	 */
	public void populateTreeWithCalculations() {
		//Add -3 and *2 as children of root to the tree.
		calculateAndAddToTree(calculationTree.getRoot());
		
		//Make new arraylist where we put bread first algorithm output
		ArrayList<TreePosition<Integer>> breadthFirstList = new ArrayList<TreePosition<Integer>>();
		
		int maxsize = ((int)(X*(Math.pow(2, treeLayers)))) + 1;
		Boolean alreadyCalculated[] = new Boolean[maxsize];
		Arrays.fill(alreadyCalculated, Boolean.FALSE);
		
		//Start populating tree.
		int i = 0;
		
		//label used to break out of do while
		do_while_label:
		do {
			breadthFirstList.clear();
			breadthFirstList = calculationTree.breadthFirst(calculationTree.getRoot(), breadthFirstList, true, 0);
			
			for(TreePosition<Integer> cursor : breadthFirstList) {
				if(cursor.getElement() >= 0 && !(alreadyCalculated[cursor.getElement()])) {
					if(cursor.getElement() == Y) {
						break do_while_label;
					}
					if(cursor.children() != null ) {
						if(cursor.children().size() == 0) {
							calculateAndAddToTree(cursor);
						}
						calculateAndAddToTree(cursor);
					}
					else {
						calculateAndAddToTree(cursor);
					}
					alreadyCalculated[cursor.getElement()] = true;
				}
			}
			
			i++;
		}while(i <= treeLayers);
		
		ArrayList<TreePosition<Integer>> listWithCalculations = new ArrayList<TreePosition<Integer>>();
		listWithCalculations = calculationTree.breadthFirst(calculationTree.getRoot(), listWithCalculations, true, 0);
		
		//Print operations if value is found
		printOutput(listWithCalculations);
	}
	
	/**
	 * Prompt user for input.
	 * Ask for starting number, number they want to reach and the amount of layers on the tree.
	 * Maximum amount of layers should not be higher than 10, otherwise there will be an out of memory error.
	 */
	private void promptUserForInput() {
		System.out.println("Enter the starting number:");
		X = reader.nextInt();
		System.out.println("Enter the number to reach:");
		Y = reader.nextInt();
		System.out.println("Enter the amount of layers on the tree:");
		treeLayers = reader.nextInt();
		reader.close();
	}
	
	/**
	 * Method that adds -3 and *2 to the tree as children of the given parent
	 * @param currentPosition
	 */
	private void calculateAndAddToTree(TreePosition<Integer> currentPosition) {
		try {
			calculationTree.addChild(currentPosition, ((int)currentPosition.getElement() -3));
			calculationTree.addChild(currentPosition, ((int)currentPosition.getElement() *2));
		} catch (InvalidTreePositionException e) {
			e.printStackTrace();
		}
		calcCounter += 2;
	}
	
	/**
	 * Traverse list for the value were looking for and return the 
	 * @param listToCheck
	 * @param valueToLookfor
	 * @return the position of the value were looking for in the tree
	 */
	private TreePosition<Integer> checkListForValue(ArrayList<TreePosition<Integer>> listToCheck, int valueToLookfor) {
		for(TreePosition<Integer> cursor : listToCheck) {
			if(cursor.getElement() == valueToLookfor) {
				return cursor;
			}
		}
		return null;
	}
	
	/**
	 * Prints the operations needed to reach the number.
	 * If no solution is found, print default string.
	 * @param listWithCalculations
	 */
	private void printOutput(ArrayList<TreePosition<Integer>> listWithCalculations) {
		ArrayList<String> operations = new ArrayList<String>();
		TreePosition<Integer> temp = checkListForValue(listWithCalculations, Y);
		//If we were able to calculate the given value.
		if(temp != null) {
			//If the value has children
			while(temp.parent() != null) {
				if(temp.parent().children().get(0) == temp) {
					//Left child is always -3 of parent.
					operations.add("-3");
				}else if(temp.parent().children().get(1) == temp) {
					//Right child is always *2 of parent.
					operations.add("*2"); 
				}
				temp = temp.parent();
			}
			//Operations are entered from start to end, so we need to reverse.
			Collections.reverse(operations);
			
			//Print all operations
			System.out.println("Going from " + X + " to " + Y + " by doing: ");
	        for(String operator : operations) {
	            System.out.print(operator + " ");
	        }
		}else {
			//Default string
			System.out.print("Number " + Y + " could not be found in " + treeLayers + " layers of the tree.");
		}	
		System.out.println("\n" + calcCounter + " calculations needed to find the value.");
	}
	
}
