# MulXSubY game

## Information
MulXSubY assignment for my algorithms course.

## What it has to do:
In the Mul2Sub3 game you have to find the minimum sequence of numbers to get from a number to a number only by multiplying by 2 or subtracting by 3.<br/>
For example from 8 to 11 you get the following sequence:<br/>
8 - 3 = 5<br/>
5 * 2 = 10<br/>
10 - 3 = 7<br/>
7 * 2 = 14<br/>
14 - 3 = 11<br/>
Implement a program which solves the MulXSubY game. X and Y should be parameters, as well as the starting and end value.<br/>
Keep track of how many calculations you need.<br/>
Optimize your search algorithm to minimize the number of calculations needed.<br/>
Describe your algorithm in a README.MD file and explain how you will minimize the number of calculations.<br/>

## What it does:
![Screenshot](pic/screenshot.PNG)

## How the algorithm works:
The user enters the starting number, the number he wants to reach and the number of layers on the tree.<br/>
Firstly, -3 and x2 of the root (starting number) get added  to the tree.<br/>
Secondly, An empty arraylist is made, this list is used to store the tree constructed by the breadthfirst algorithm.<br/>
lastly, A boolean array of size [startnumber x (2^treelayers) + 1] is made and initialized to false.<br/>
Now the populating of the tree is started.<br/>

Firstly, the breadth first algorithm returns the tree as an arraylist.<br/>
This list is now iterated.<br/>
If the iterated value (cursor) is greater than zero and if it is nog yet calculated (-3 and x2), then it is check for:<br/>
if the cursor is the value we are looking for, then stop the populating loop.<br/>
if the cursor does not have children, add them.<br/>

After calculating and adding -3 and x2 of the cursor to the tree, change status of cursor in boolean array to true.<br/>

After the populating is done, we determine if the value is found.<br/>
If the value is found, print out all the operations.<br/>


## Sources
- Emiel Estievenart<br/>
- https://stackoverflow.com/questions/30554861/multiply-add-initial-number-to-get-target-number<br/>
